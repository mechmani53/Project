package web.tyss.com.scripts;

import org.testng.annotations.Test;

import web.tyss.com.baseutil.BaseTest;
import web.tyss.com.baseutil.InitializePages;
import web.tyss.com.dataproviders.DataProviderFactory;
import web.tyss.com.dataproviders.DataProviderFileRowFilter;


public class Nike extends BaseTest{
		

		@DataProviderFileRowFilter(file = "./src/main/resources/data/Data.xlsx", sql = "Select * from Nike where SlNo ='1'")
		@Test(dataProvider = "data1", dataProviderClass = DataProviderFactory.class, description = "Description: Verify that whether user is able to Add the Product")
		public void shopAtNike(String SlNo,String mainMenu,String mainMenu2,String subMenu,String subCategory,String productName,String size,String priceRange) throws Exception {
		InitializePages pages = new InitializePages(driver, ETO, WebActionUtil);
	
		System.out.println("Hello");
		/*verify on main menu */
		pages.nikePage.verifyMainMenu(mainMenu);
		
		/*click on main menu*/
		pages.nikePage.mouseHoverOnMainMenu(mainMenu);

		/*verify on main menu */
		pages.nikePage.verifysubMenu(subMenu);
		
		/*click on sub menu */
		pages.nikePage.clkSubmenu(subMenu);

		/*Clk subCategory*/
		pages.nikePage.clkSubCategory(subCategory);
		
		/*click on sort by*/
		pages.nikePage.clkSortByDropdown();
		
		/*click on price */
		pages.nikePage.selectPriceRange(priceRange);
		
//		/*get all products */
//		pages.nikePage.getAllProducts();
		
		/*click on product */
		pages.nikePage.clkOnProduct(productName);
		
		/*click on product title*/
		pages.nikePage.verifyPrduct(productName);
		
		/*click on size*/
		pages.nikePage.selectProdSize(size);
		
		/*click on add to bag */
		pages.nikePage.clkOnAddToBag();
		
		/*verify cart */
		pages.nikePage.verifyCartBtn();
		
		/*click on cart */
		pages.nikePage.clkOnCartBtn();
		
		/*click on price */
		pages.nikePage.verifySizeofTheProd(size);
		
		

}

}
