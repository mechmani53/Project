package web.tyss.com.util.commonutils;


import java.io.FileInputStream;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import web.tyss.com.baseutil.BaseTest;




/**
 * Description: Implements generic methods to perform operations on Excel Workbook.
 * @author: Manikandan A
 */
public class ExcelUtil{
	/* Private constructor*/
	private ExcelUtil() {

	}

	
	
	/**
	 * Description: Implements fetching Row Data from Excel using Fillo
	 * 
	 * @author: Manikandan A
	 * @param xlPath
	 */
	public static synchronized String[][] getRowDataFromExcelUsingFillo(String xlPath, String sql) {

		Fillo fillo = new Fillo();
		String[][] tableValues = null;
		Connection connection;
		try {
			connection = fillo.getConnection(xlPath);
			Recordset recordset = connection.executeQuery(sql);
		
			ArrayList<String> lstFieldNames = recordset.getFieldNames();


			tableValues = new String[recordset.getCount()][lstFieldNames.size()];

			int rowno = 0;

			while (recordset.next()) {

				int colno = 0;

				for (String fieldName : lstFieldNames) {
					tableValues[rowno][colno++] = recordset.getField(fieldName);
					

				}
				rowno++;

			}

			recordset.close();
			connection.close();
		} catch (FilloException e) {
			// TODO Auto-generated catch block
			System.out.println("Fillo exception");
			e.printStackTrace();
		}

		return tableValues;
	}

}
