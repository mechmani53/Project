package web.tyss.com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import web.tyss.com.util.WebActionUtil;

public class AdditionalDetails_Page {
	public static WebDriver driver;
	public WebActionUtil WebActionUtil;
	public long ETO = 10;
	
	public AdditionalDetails_Page(WebDriver driver, long ETO, WebActionUtil WebActionUtil) 
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.WebActionUtil = WebActionUtil;
		this.ETO = ETO;
	}	
	
	/*Professional Description text area*/
	@FindBy(xpath="//textarea[@id='title']")
	private WebElement taProfessionalDescription;
	
	/*Preffered Work Location drop down*/
	@FindBy(xpath="//div[@id='preferredLoc']//input[@class='sugInp clicked']")
	private WebElement ddPrefferedWorkLocation;
	
	/*Selecting Preffered Work Location drop down*/
	public static WebElement selectingOptionUnderPrefferedWorkLocation(String workLocation) 
	{
	String xpath = "//ul[@class='sugCont listing slideDown']/li//span[contains(text(),'"+workLocation+"')]";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Selecting Desired Job Type tab*/
	public static WebElement selectingDesiredJobType(String jopType) 
	{
	String xpath = "//label[text()='Desired Job Type']/following-sibling::div//label[@data-content='"+jopType+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Selecting Desired Employment Type tab*/
	public static WebElement selectingDesiredEmploymentType(String employementType) 
	{
	String xpath = "//label[text()='Desired Employment Type']/following-sibling::div//label[@data-content='"+employementType+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Selecting Gender tab*/
	public static WebElement selectingGender(String gender) 
	{
	String xpath = "//label[text()='Gender']/following-sibling::div//label[@data-content='"+gender+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*IT Skills text box*/
	@FindBy(name="skill0")
	private WebElement tbItSkills;
	
	/*IT Skills version text box*/
	@FindBy(name="version0")
	private WebElement tbItSkillsVersion;
	
	
	/*IT Skills Last Used Years drop down*/
	@FindBy(name="lastUsedYear0")
	private WebElement ddItSkillsLastUsedYear;
	
	/*Selecting Last Used Years from drop down*/
	public static WebElement selectingLastUsedYear(String year) 
	{
	String xpath = "//div[@class='transcluded']/span[text()='"+year+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*IT Skills Experience drop down*/
	@FindBy(name="expYear0")
	private WebElement ddItSkillsExperience;
	
	/*Selecting Last Used Years from drop down*/
	public static WebElement selectingExperience(String experienceYear) 
	{
	String xpath = "//div[@class='transcluded']/span[text()='"+experienceYear+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	@FindBy(xpath="//button[@name='submitProfileComplete']")
	private WebElement btnSubmit;
	
	/*Validating the Name*/
	public static WebElement validatingName(String name) 
	{
	String xpath = "//div[text()='"+name+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/** 
	 * Description: Method to select the Professional Description
	 * @author Manikandan A
	 * @param content
	 * */
	public void setProfessionalDescription(String content)
	{
		try
		{
			WebActionUtil.waitForElement(taProfessionalDescription, "Professional Description text box",5);
			WebActionUtil.typeText(taProfessionalDescription, content, "Professional Description text box");
			String actualData=taProfessionalDescription.getAttribute("value");
			WebActionUtil.validateEnteredData(content, actualData,"Professional Description Text box",content + " Professional DescriptionText box", content +" not entered in Professional Description Text box");
				
			
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Professional Description");
			Assert.fail("Unable to Set Professional Description");
			
		}
	}
	
	/** 
	 * Description: Method to select the Preferred Location
	 * @author Manikandan A
	 * @param location
	 * */
	public void setPreferredLocation(String location)
	{
		try
		{
			WebActionUtil.waitForElement(ddPrefferedWorkLocation, "Preffered Location drop down",5);
			WebActionUtil.clickOnWebElement(ddPrefferedWorkLocation, "Preffered Location drop down", "unable to click on Preffered Location drop down");
			
			
			WebActionUtil.isElementClickable(selectingOptionUnderPrefferedWorkLocation(location), "Options under Preffered Location drop down" );
			WebActionUtil.clickOnWebElement(selectingOptionUnderPrefferedWorkLocation(location), "Options under Preffered Location drop down", "Unable to click on Options of Preffered Location drop down");
			WebActionUtil.isElementDisplayed(selectingOptionUnderPrefferedWorkLocation(location), " Preffered Location drop down");
			
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Preffered Location");
			Assert.fail("Unable to Set Preffered Location");
			
		}
	}
	
	/** 
	 * Description: Method to select the Job Type
	 * @author Manikandan A
	 * @param jobType
	 * */
	public void setDesiredJopType(String jobType)
	{
		try
		{
					
			WebActionUtil.isElementClickable(selectingDesiredJobType(jobType), "Desired Job Type button" );
			WebActionUtil.clickOnWebElement(selectingDesiredJobType(jobType), "Desired Job Type button", "Unable to click on Desired Job Type button");
			WebActionUtil.isElementDisplayed(selectingDesiredJobType(jobType), " Desired Job Type button");
			
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Preffered Location");
			Assert.fail("Unable to Set Preffered Location");
			
		}
	}
	/** 
	 * Description: Method to select the Employment Type
	 * @author Manikandan A
	 * @param employmentType
	 * */
	public void setDesiredEmploymentType(String employmentType)
	{
		try
		{
					
			WebActionUtil.isElementClickable(selectingDesiredEmploymentType(employmentType), "Employment Type button" );
			WebActionUtil.clickOnWebElement(selectingDesiredEmploymentType(employmentType), "Employment Type button", "Unable to click on Employment Type button");
			WebActionUtil.isElementDisplayed(selectingDesiredEmploymentType(employmentType), " EmploymentType button");
			
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Employment");
			Assert.fail("Unable to Set Employment");
			
		}
	}
	
	/** 
	 * Description: Method to set IT Skills
	 * @author Manikandan A
	 * @param itSkills
	 * */
	public void setItSkills(String itSkills)
	{
		try
		{
			WebActionUtil.waitForElement(tbItSkills, "IT Skills Text Box", 5);
			WebActionUtil.typeText(tbItSkills, itSkills,"Current Company Text Box");
			String actualData=tbItSkills.getAttribute("value");
			WebActionUtil.validateEnteredData(itSkills, actualData,"IT Skills Text box",itSkills + " IT Skills Text box", itSkills +" not entered in IT Skills Text box");
		}
		
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set IT Skills");
			Assert.fail("Unable to Set IT Skills");
			
		}
	}
	
	/** 
	 * Description: Method to set the IT Skills Version
	 * @author Manikandan A
	 * @param itSkillsVersion
	 * */
	public void setItSkillsVersion(String itSkillsVersion)
	{
		try
		{
			WebActionUtil.waitForElement(tbItSkillsVersion, "IT Skills Version Text Box", 5);
			WebActionUtil.typeText(tbItSkillsVersion, itSkillsVersion,"IT Skills Version Text Box");
			String actualData=tbItSkillsVersion.getAttribute("value");
			WebActionUtil.validateEnteredData(itSkillsVersion, actualData,"IT Skills Version Text box",itSkillsVersion + " IT Skills Version Text box", itSkillsVersion +" not entered in IT Skills Version Text box");
		}
		
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set IT Skills Version");
			Assert.fail("Unable to Set IT Skills Version");
			
		}
	}
	
	/** 
	 * Description: Method to select the Last Used Year and validation
	 * @author Manikandan A
	 * @param year
	 * */
	public void setLastUsedYear(String year)
	{
		try
		{
			WebActionUtil.waitForElement(ddItSkillsLastUsedYear, "Last Used Year drop down", 5);
			WebActionUtil.clickOnWebElement(ddItSkillsLastUsedYear, "Last Used Year drop down", "unable to click on Last Used Year drop down");
			
			WebActionUtil.isElementClickable(selectingLastUsedYear(year), "Options under Last Used Year drop down" );
			WebActionUtil.clickOnWebElement(selectingLastUsedYear(year), "Options under Last Used Year drop down", "Unable to click on Options of Last Used Year drop down");
			WebActionUtil.isElementDisplayed(selectingLastUsedYear(year), " Last Used Year drop down");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Last Used Year");
			Assert.fail("Unable to Set Last Used Year");
			
		}
	}
	
	/** 
	 * Description: Method to select the Experience Year
	 * @author Manikandan A
	 * @param experienceYear
	 * */
	public void setExperienceYear(String experienceYear)
	{
		try
		{
			WebActionUtil.waitForElement(ddItSkillsExperience, "Experience Year drop down", 5);
			WebActionUtil.clickOnWebElement(ddItSkillsExperience, "Experience Year drop down", "unable to click on Experience Year drop down");
			
			WebActionUtil.isElementClickable(selectingExperience(experienceYear), "Options under Experience Year drop down" );
			WebActionUtil.clickOnWebElement(selectingExperience(experienceYear), "Options under Experience Year drop down", "Unable to click on Options of Experience Year drop down");
			WebActionUtil.isElementDisplayed(selectingExperience(experienceYear), "Experience Year drop down");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Experience Year Year");
			Assert.fail("Unable to Set Experience Year");
			
		}
	}
	
	/** 
	 * Description: Method to select the Gender
	 * @author Manikandan A
	 * @param gender
	 * */
	public void setGender(String gender)
	{
		try
		{
			WebActionUtil.isElementClickable(selectingGender(gender), "Gender button" );
			WebActionUtil.clickOnWebElement(selectingGender(gender), "Gender button", "Unable to click on Gender button");
			WebActionUtil.isElementDisplayed(selectingGender(gender), "Gender button");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Gender");
			Assert.fail("Unable to Set Gender");
			
		}
	}
	
	/** 
	 * Description: Method to click on submit button
	 * @author Manikandan A
	 * */
	public void clkOnSubmitButton()
	{
		try
		{
			WebActionUtil.isElementClickable(btnSubmit, "Submit button" );
			WebActionUtil.clickOnWebElement(btnSubmit, "Submit button", "Unable to click on Submit button");
			WebActionUtil.isElementDisplayed(btnSubmit, "Submit button");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to click on Submit button");
			Assert.fail("Unable to click on Submit button");
			
		}
	}
	/** 
	 * Description: Method to validate the account registration
	 * @author Manikandan A
	 * @param name
	 * */
	public void validateCreationOfAccount(String name)
	{
		try
		{
			WebActionUtil.waitForElement(validatingName(name), "Verifiying Name", 15);
			WebActionUtil.isElementDisplayed(validatingName(name), "Verifiying Name");
			String actualData=validatingName(name).getText();
			WebActionUtil.validateEnteredData(name, actualData,"Verifiying Name",name + " is Verified", name +" is not Verified");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Verify Name");
			Assert.fail("Unable to Verify Name");
			
		}
	}
}
