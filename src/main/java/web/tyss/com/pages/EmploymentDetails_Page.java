package web.tyss.com.pages;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import web.tyss.com.util.WebActionUtil;

public class EmploymentDetails_Page {
	
	public static WebDriver driver;
	public WebActionUtil WebActionUtil;
	public long ETO = 10;
	
	public EmploymentDetails_Page(WebDriver driver, long ETO, WebActionUtil WebActionUtil) 
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.WebActionUtil = WebActionUtil;
		this.ETO = ETO;
	}	
	
	/*Current Designation text box*/
	@FindBy(name="designation_0")
	private WebElement tbCurrentDesignation;
	
	/*Current Company text box*/
	@FindBy(name="company_0")
	private WebElement tbCurrentCompany;
	
		
	/*Selecting Company Industry drop down*/
	public static WebElement selectingSkills(String skills) 
	{
	String xpath = "//li//span[text()='"+skills+"']";
	return driver.findElement(By.xpath(xpath));
	}
	/*Company Industry drop down*/
	@FindBy(name="industry")
	private WebElement ddCompanyIndustry;
	
	/*Selecting Company Industry drop down*/
	public static WebElement selectingIndustryUnderIndustryDd(String industry) 
	{
	String xpath = "//div[@id='industry']//div[@class='dd_dwn']/ul/li//span[text()='"+industry+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Functional Area drop down*/
	@FindBy(name="farea")
	private WebElement ddFunctionalArea;
	
	/*Selecting Functional Area drop down*/
	public static WebElement selectingAreaUnderFunctionalAreaDd(String area) 
	{
	String xpath = "//div[@class='dd_dwn']/ul/li//span[text()='"+area+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Role drop down*/
	@FindBy(name="role")
	private WebElement ddRole;
	
	/*Selecting Role drop down*/
	public static WebElement selectingRoleUnderRoleDd(String role) 
	{
	String xpath = "//div[@class='dd_dwn']/ul/li//span[contains(text(),'"+role+"')]";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Salary drop down in Lakhs*/
	@FindBy(xpath="//div[@class='salary ddwn']//input[@name='salaryLac_0']")
	private WebElement ddSalaryLakhs;
	
	/*Salary drop down in Thousands*/
	@FindBy(xpath="//span[@class='ddIcon srchTxt']/following-sibling::input[@name='salaryThou_0']")
	private WebElement ddSalaryThousands;
	
	
	/*Selecting Salary drop down in Lakhs*/
	public static WebElement selectingLakhsUnderSalaryLakhsDd(String lakhs) 
	{
	String xpath = "//input[@name='salaryLac_0']/ancestor::div[@class='salary ddwn']//ul[@class='droopeCont listing']/li//span[text()='"+lakhs+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Selecting Salary drop down in Thousands*/
	public static WebElement selectingThousandsUnderSalaryThousandsDd(String thousands) 
	{
	String xpath = "//span[text()='"+thousands+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	
	/*Selecting Currency Type drop down */
	public static WebElement selectingCurrencyType(String currency) 
	{
	String xpath = "//div[@class='row']//input[@id='radio1_0']/following-sibling::label[@data-text='"+currency+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*From Year under Current Company*/
	@FindBy(name="startYear_0")
	private WebElement ddFromYearUnderCurrentCompany;
	
	/*From Month under Current Company*/
	@FindBy(name="startMonth_0")
	private WebElement ddFromMonthUnderCurrentCompany;
	
	/*To Year under Current Company*/
	@FindBy(name="endYear_0")
	private WebElement ddToYearUnderCurrentCompany;
	
	/*To Month under Current Company*/
	@FindBy(name="endMonth_0")
	private WebElement ddToMonthUnderCurrentCompany;
	
	/*Selecting From Year under Current Company*/
	public static WebElement selectingFromYearUnderCurrentCompany(String year) 
	{
	String xpath = "//input[@name='startYear_0']/ancestor::div[@class='DDwrap']/following-sibling::div//ul/li//span[text()='"+year+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Selecting from Month under Current Company*/
	public static WebElement selectingFromMonthUnderCurrentCompany(String month) 
	{
	String xpath = "//input[@name='startMonth_0']/ancestor::div[@class='DDwrap']/following-sibling::div//ul/li//span[text()='"+month+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Selecting To Year under Current Company*/
	public static WebElement selectingToYearUnderCurrentCompany(String year) 
	{
	String xpath = "//input[@name='endYear_0']/ancestor::div[@class='DDwrap']/following-sibling::div//ul/li//span[text()='"+year+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Selecting To Month under Current Company*/
	public static WebElement selectingToMonthUnderCurrentCompany(String month) 
	{
	String xpath = "//input[@name='endMonth_0']/ancestor::div[@class='DDwrap']/following-sibling::div//ul/li//span[text()='"+month+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Current City text box*/
	@FindBy(xpath="//div[@class='errorCover locationSugg']//input[@class='sugInp clicked']")
	private WebElement tbCurrentCity;
	
	/*State drop down*/
	@FindBy(name="state")
	private WebElement ddState;
	
	/*Selecting State from drop down*/
	public static WebElement selectingState(String state) 
	{
	String xpath = "//input[@name='state']/ancestor::div[@class='DDwrap']/following-sibling::div//ul/li//span[text()='"+state+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Selecting Current city from drop down*/
	public static WebElement selectingCurrentCity(String currentCity) 
	{
	String xpath = "//ul[@class='sugCont listing slideDown']/li//span[contains(text(),'"+currentCity+"')]";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Duration of Notice Period drop down*/
	@FindBy(name="noticePeriod")
	private WebElement ddNoticePeriod;
	
	/*Selecting Duration of Notice Period drop down*/
	public static WebElement selectingNoticePeriod(String noticePeriod) 
	{
	String xpath = "//input[@name='noticePeriod']/ancestor::div[@class='DDwrap']/following-sibling::div//ul/li//span[text()='"+noticePeriod+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	
	@FindBy(xpath="//input[@name='keyskills']/ancestor::div[@class='sWrap']/following-sibling::ul/li//span")
	private String optKeySkill;
	
	/*Designation text box*/
	@FindBy(name="designation_1")
	private WebElement tbFirstDesignation;
	
	/*Company Name text box*/
	@FindBy(name="company_1")
	private WebElement tbFirstCompany;
	
	/*Designation text box*/
	@FindBy(name="designation_2")
	private WebElement tbSecondDesignation;
	
	/*Company Name text box*/
	@FindBy(name="company_2")
	private WebElement tbSecondCompany;
	
	/*Add Another Employment link*/
	@FindBy(xpath="//a[text()='+add another employment']")
	private WebElement lnkAddAnotherEmployement;
	
	/*Submit button*/
	@FindBy(xpath="//button[@name='submitEmploymentDetail']")
	private WebElement btnContinue;
	
	/** 
	 * Description: Method to Fill Current Designation
	 * @author Manikandan A
	 * @param currentDesignation
	 */
	public void setCurrentDesignation(String currentDesignation)
	{
		try
		{
			WebActionUtil.waitForElement(tbCurrentDesignation, "Current Designation Text Box", 15);
			WebActionUtil.isElementVisible(tbCurrentDesignation, "Current Designation Text Box");
			WebActionUtil.typeText(tbCurrentDesignation, currentDesignation,"Current Designation Text Box");
			String actualData=tbCurrentDesignation.getAttribute("value");
			WebActionUtil.validateEnteredData(currentDesignation, actualData,"Current Designation Text box",currentDesignation + " Current Designation Text box", currentDesignation +" Current Designation Text box");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to enter details on Current Designation Text Box");
			Assert.fail("Unable to enter details on Current Designation Text Box");
		}
	}
	
	/** 
	 * Description: Method to Fill Current Designation
	 * @author Manikandan A
	 * @param currentCompany
	 */
	public void setCurrentCompany(String currentCompany)
	{
		try
		{
			WebActionUtil.waitForElement(tbCurrentCompany, "Current Company Text Box", 15);
			WebActionUtil.isElementVisible(tbCurrentCompany, "Current Company Text Box");
			WebActionUtil.typeText(tbCurrentCompany, currentCompany,"Current Company Text Box");
			String actualData=tbCurrentCompany.getAttribute("value");
			WebActionUtil.validateEnteredData(currentCompany, actualData,"Current Company Text box",currentCompany + " Current Company Text box", currentCompany +" Current Company Text box");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to enter details on Current Company Text Box");
			Assert.fail("Unable to enter details on Current Company Text Box");
		}
	}
	
	/** 
	 * Description: Method to select the salary and validating
	 * @author Manikandan A
	 * @param currency
	 * @param lakhs
	 * @param thousands
	 */	
	public void setAnnualSalary(String currency,String lakhs,String thousands)
	{
		try
		{
			WebActionUtil.waitForElement(selectingCurrencyType(currency), "Currency Radio button", 15);
			WebActionUtil.isElementClickable(selectingCurrencyType(currency),"Currency Radio button");
			WebActionUtil.clickOnWebElement(selectingCurrencyType(currency), "Currency Radio Button", "unable to click on Currency Radio Button");
			WebActionUtil.isElementDisplayed(selectingCurrencyType(currency), "Currency Radio Button");	
			
			WebActionUtil.waitForElement(ddSalaryLakhs, "Salary in Lakhs Drop down", 15);
			WebActionUtil.isElementClickable(ddSalaryLakhs,"Salary in Lakhs Drop down");
			WebActionUtil.clickOnWebElement(ddSalaryLakhs, "Salary in Lakhs Drop down", "unable to click on Salary In Lakhs Drop down");
			
			WebActionUtil.waitForElement(selectingLakhsUnderSalaryLakhsDd(lakhs), "Selecting Salary in Lakhs Drop down", 30);
			WebActionUtil.isElementClickable(selectingLakhsUnderSalaryLakhsDd(lakhs),"Selecting Salary in Lakhs Drop down");
			WebActionUtil.clickOnWebElement(selectingLakhsUnderSalaryLakhsDd(lakhs),"Option under Salary In Lakhs Drop down","Unable to click on Option under Salary In Lakhs Drop down");
			WebActionUtil.isElementDisplayed(selectingLakhsUnderSalaryLakhsDd(lakhs), "Selecting Salary in Lakhs Drop down");
			
			WebActionUtil.waitForElement(ddSalaryThousands, "Salary in Thousands drop down", 20);
			WebActionUtil.isElementClickable(ddSalaryThousands,"Salary in Thousands Drop down");
			WebActionUtil.clickOnWebElement(ddSalaryThousands, "Salary in Thousands Drop down", "unable to click on Salary In Thousands Drop down");
			
			WebActionUtil.waitForElement(selectingThousandsUnderSalaryThousandsDd(thousands), "Salary in Thousands drop down", 45);
			WebActionUtil.isElementClickable(selectingThousandsUnderSalaryThousandsDd(thousands),"Salary in Thousands Drop down");
			WebActionUtil.clickOnWebElement(selectingThousandsUnderSalaryThousandsDd(thousands),"Option under Salary in Thousands Drop down","Unable to click on Option under Salary In Thousands Drop down");
			WebActionUtil.isElementDisplayed(selectingThousandsUnderSalaryThousandsDd(thousands), "Salary in Thousands drop down");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("unable to set the Annual Salary ");
			Assert.fail("unable to set the Annual Salary");
		}
	}
	
	/** 
	 * Description: Method to select the work Experience
	 * @author Manikandan A
	 * @param fromYear
	 * @param fromMonth
	 * @param toYear
	 * @param toMonth
	 */	
	public void setWorkingExperience(String fromYear,String fromMonth,String toYear,String toMonth)
	{
		try
		{
			WebActionUtil.waitForElement(ddFromYearUnderCurrentCompany, "From Year Drop Down under Current Company", 15);
			WebActionUtil.isElementClickable(ddFromYearUnderCurrentCompany,"From Year Drop Down under Current Company");
			WebActionUtil.clickOnWebElement(ddFromYearUnderCurrentCompany, "From Year Drop Down under Current Company", "unable to click on From Year Drop Down under Current Company");
			
			WebActionUtil.waitForElement(selectingFromYearUnderCurrentCompany(fromYear), "From Year Drop Down under Current Company", 45);
			WebActionUtil.isElementClickable(selectingFromYearUnderCurrentCompany(fromYear),"From Year Drop Down under Current Company");
			WebActionUtil.clickOnWebElement(selectingFromYearUnderCurrentCompany(fromYear),"Option under From Year Drop Down in Current Company","Unable to click on Option under From Year Drop Down in Current Company");
			WebActionUtil.isElementDisplayed(selectingFromYearUnderCurrentCompany(fromYear), "From Year Drop Down under Current Company");
			
			WebActionUtil.waitForElement(ddFromMonthUnderCurrentCompany, "From Month Drop Down under Current Company", 15);
			WebActionUtil.isElementClickable(ddFromMonthUnderCurrentCompany,"From Month Drop Down under Current Company");
			WebActionUtil.clickOnWebElement(ddFromMonthUnderCurrentCompany, "From Month Drop Down under Current Company", "unable to click on From Month Drop Down under Current Company");
			
			WebActionUtil.waitForElement(selectingFromMonthUnderCurrentCompany(fromMonth), "From Month Drop Down under Current Company", 45);
			WebActionUtil.clickOnWebElement(selectingFromMonthUnderCurrentCompany(fromMonth),"Option under From Month Drop Down in Current Company","Unable to click on Option under From Month Drop Down in Current Company");
			WebActionUtil.isElementDisplayed(selectingFromMonthUnderCurrentCompany(fromMonth), "From Month Drop Down under Current Company");
			
			WebActionUtil.waitForElement(ddToYearUnderCurrentCompany, "To Year Drop Down under Current Company", 15);
			WebActionUtil.isElementClickable(ddToYearUnderCurrentCompany,"To Year Drop Down under Current Company");
			WebActionUtil.clickOnWebElement(ddToYearUnderCurrentCompany, "To Year Drop Down under Current Company", "unable to click on To Year Drop Down under Current Company");
			
			WebActionUtil.waitForElement(selectingToYearUnderCurrentCompany(toYear), "Option under To Year Drop Down in Current Company", 35);
			WebActionUtil.clickOnWebElement(selectingToYearUnderCurrentCompany(toYear),"Option under To Year Drop Down in Current Company","Unable to click on Option under To Year Drop Down in Current Company");
			WebActionUtil.isElementDisplayed(selectingToYearUnderCurrentCompany(toYear), "Option under To Year Drop Down in Current Company");
		
			if(!toYear.equals("Present"))
			{
				WebActionUtil.waitForElement(ddToMonthUnderCurrentCompany, "To Month Drop Down under Current Company", 45);
				WebActionUtil.isElementClickable(ddToMonthUnderCurrentCompany, "To Month Drop Down under Current Company");
				WebActionUtil.clickOnWebElement(ddToMonthUnderCurrentCompany, "To Month Drop Down under Current Company", "unable to click on To Month Drop Down under Current Company");
				
				WebActionUtil.waitForElement(selectingToMonthUnderCurrentCompany(toMonth), "Option under To Month Drop Down in Current Company", 45);
				WebActionUtil.isElementClickable(selectingToMonthUnderCurrentCompany(toMonth), "Option under To Month Drop Down in Current Company");
				WebActionUtil.clickOnWebElement(selectingToMonthUnderCurrentCompany(toMonth),"Option under To Month Drop Down in Current Company","Unable to click on Option under To Month Drop Down in Current Company");
				WebActionUtil.isElementDisplayed(selectingToMonthUnderCurrentCompany(toMonth), "Option under To Month Drop Down in Current Company");
			}
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("unable to set the Working Experience ");
			Assert.fail("unable to set the Working Experience");
		}
		
	}
	
	/** 
	 * Description: Method to select the Current City
	 * @author Manikandan A
	 * @param currentCity
	 */	
	public void setCurrentCity(String currentCity)
	{
		try
		{
			WebActionUtil.waitForElement(selectingCurrentCity(currentCity), "Current City Text Box", 15);
			WebActionUtil.clickOnWebElement(selectingCurrentCity(currentCity), "Option in Current City Drop Down", "Unable to click on Option in Current City Drop Down");
			WebActionUtil.isElementDisplayed(selectingCurrentCity(currentCity), "Current City Drop Down");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("unable to set the Current City ");
			Assert.fail("unable to set the Current City");
		}
		
		
	}
	
	/** 
	 * Description: Method to select the State
	 * @author Manikandan A
	 * @param state
	 */	
	public void setCurrentState(String state)
	{
		try
		{
			WebActionUtil.waitForElement(ddState, "State Drop Down", 15);
			WebActionUtil.isElementClickable(ddState, "State Drop Down");
			WebActionUtil.clickOnWebElement(ddState, "State Drop Down", "Unable to click on State Drop Down");
			
			WebActionUtil.waitForElement(selectingState(state), "State Drop Down", 15);
			WebActionUtil.clickOnWebElement(selectingState(state), "Option in State Drop Down", "Unable to click on Option in State Drop Down");
			WebActionUtil.isElementDisplayed(selectingState(state), "State Drop Down");
			
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("unable to set the Current State ");
			Assert.fail("unable to set the Current State");
		}
		
		
	}
	
	/** 
	 * Description: Method to select the Notice Period
	 * @author Manikandan A
	 * @param noticePeriod
	 */	
	public void setNoticePeriod(String noticePeriod)
	{
		try
		{
			WebActionUtil.waitForElement(ddNoticePeriod, "Notice Period Drop Down", 15);
			WebActionUtil.isElementClickable(ddNoticePeriod, "Notice Period Drop Down");
			WebActionUtil.clickOnWebElement(ddNoticePeriod, "Notice Period Drop Down", "unable to click on Notice Period Drop Down");
			
			WebActionUtil.waitForElement(selectingNoticePeriod(noticePeriod), "Notice Period Drop Down", 15);
			WebActionUtil.clickOnWebElement(selectingNoticePeriod(noticePeriod), "Option in Notice Period Drop Down", "unable to click on Option in Notice Period Drop Down");
			WebActionUtil.isElementDisplayed(selectingNoticePeriod(noticePeriod), "State Drop Down");
			
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("unable to set the Notice Period");
			Assert.fail("unable to set the Notice Period");
		}
		
		
	}
	
	/** 
	 * Description: Method to enter Skills
	 * @author Manikandan A
	 * @param skills
	 */	
	public void setSkills(String skills)
	{
		try
		{
			WebActionUtil.waitForElement(selectingSkills(skills), "Skills text box", 15);
			WebActionUtil.clickOnWebElement(selectingSkills(skills), "Option in Skills Drop Down", "unable to click on Option in Skills Drop Down");
			WebActionUtil.isElementDisplayed(selectingSkills(skills), "Skills Drop Down");
			
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("unable to set the Skills");
			Assert.fail("unable to set the Skills");
		}
		
		
	}
	/** 
	 * Description: Method to select the Company Industry
	 * @author Manikandan A
	 * @param industry
	 */	
	public void setCompanyIndustry(String industry)
	{
		try
		{
			WebActionUtil.waitForElement(ddCompanyIndustry, "Company Industry Drop Down", 15);
			WebActionUtil.isElementClickable(ddCompanyIndustry, "Company Industry Drop Down");
			WebActionUtil.clickOnWebElement(ddCompanyIndustry, "Company Industry Drop Down", "unable to click on Company Industry Drop Down");
			
			WebActionUtil.waitForElement(selectingIndustryUnderIndustryDd(industry), "Company Industry Drop Down", 15);
			WebActionUtil.clickOnWebElement(selectingIndustryUnderIndustryDd(industry), "Option in Company Industry Drop Down", "unable to click on Option in Company Industry Drop Down");
			WebActionUtil.isElementDisplayed(selectingIndustryUnderIndustryDd(industry), "Company Industry Drop Down");
			
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("unable to set the Company Industry");
			Assert.fail("unable to set the Company Industry");
		}
		
		
	}
	
	/** 
	 * Description: Method to select the Functional Area
	 * @author Manikandan A
	 * @param area
	 */	
	public void setFunctionalArea(String area)
	{
		try
		{
			WebActionUtil.waitForElement(ddFunctionalArea, "Functional Area Drop Down", 15);
			WebActionUtil.isElementClickable(ddFunctionalArea, "Functional Area Drop Down");
			WebActionUtil.clickOnWebElement(ddFunctionalArea, "Functional Area Drop Down", "unable to click on Functional Area Drop Down");
			
			WebActionUtil.waitForElement(selectingAreaUnderFunctionalAreaDd(area), "Functional Area Drop Down", 15);
			WebActionUtil.clickOnWebElement(selectingAreaUnderFunctionalAreaDd(area), "Option in Functional Area Drop Down", "unable to click on Option in Functional Area Drop Down");
			WebActionUtil.isElementDisplayed(selectingAreaUnderFunctionalAreaDd(area), "Functional Area Drop Down");
			
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("unable to set the Functional Area");
			Assert.fail("unable to set the Functional Area");
		}
		
		
	}
	
	/** 
	 * Description: Method to select the Role
	 * @author Manikandan A
	 * @param role
	 */	
	public void setRole(String role)
	{
		try
		{
			WebActionUtil.waitForElement(ddRole, "Role Drop Down", 15);
			WebActionUtil.isElementClickable(ddRole, "Role Drop Down");
			WebActionUtil.clickOnWebElement(ddRole, "Role Drop Down", "unable to click on Role Drop Down");
			
			WebActionUtil.waitForElement(selectingRoleUnderRoleDd(role), "Role Drop Down", 15);
			WebActionUtil.clickOnWebElement(selectingRoleUnderRoleDd(role), "Option in Role Drop Down", "unable to click on Option in Role Drop Down");
			WebActionUtil.isElementDisplayed(selectingRoleUnderRoleDd(role), "Role Drop Down");
			
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("unable to set the Role");
			Assert.fail("unable to set the Role");
		}
		
		
	}
	
	/** 
	 * Description: Method to select the Role
	 * @author Manikandan A
	 */	
	public void clkOnContinueButton()
	{
		try
		{
			WebActionUtil.waitForElement(btnContinue, "Continue Button", 15);
			WebActionUtil.clickOnWebElement(btnContinue, "Continue Button", "unable to click on Continue Button ");
						
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("unable to click Continue Button");
			Assert.fail("unable to click Continue Button");
		}
		
		
	}
	
}
