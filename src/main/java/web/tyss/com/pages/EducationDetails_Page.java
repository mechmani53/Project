package web.tyss.com.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import web.tyss.com.util.WebActionUtil;

public class EducationDetails_Page {
	public static WebDriver driver;
	public WebActionUtil WebActionUtil;
	public long ETO = 10;
	
	public EducationDetails_Page(WebDriver driver, long ETO, WebActionUtil WebActionUtil) 
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.WebActionUtil = WebActionUtil;
		this.ETO = ETO;
	}	
	/*Add More Education*/
	@FindBy(xpath="//a[text()='+ Add more education']")
	private WebElement lnkAddMoreEducation;
	
	/*Button to Continue*/
	@FindBy(xpath="//button[@name='submitEducationDetail']")
	private WebElement btnContinue;
	
	/*Highest Qualification drop down*/
	@FindBy(name="qualification_0")
	private WebElement ddHighestQualification;
	
	/*Course drop down*/
	@FindBy(name="course_0")
	private WebElement ddCourse;
	
	/*Specialization drop down*/
	@FindBy(name="spec_0")
	private WebElement ddSpecialization;
	
	/*Selecting Highest Qualification drop down*/
	public static WebElement selectingHighestQualification(String highestQualification) 
	{
	String xpath = "//input[@name='qualification_0']/ancestor::div[@class='DDwrap']/following-sibling::div//li//span[text()='"+highestQualification+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Selecting Course drop down*/
	public static WebElement selectingCourse(String course) 
	{
	String xpath = "//input[@name='course_0']/ancestor::div[@class='DDwrap']/following-sibling::div//li//span[text()='"+course+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Selecting Specialization drop down*/
	public static WebElement selectingSpecialization(String specialization) 
	{
	String xpath = "//input[@name='spec_0']/ancestor::div[@class='DDwrap']/following-sibling::div//li//span[text()='"+specialization+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*University or  College drop down*/
	@FindBy(name="institute_0")
	private WebElement tbUniversityOrCollege;
	
	/*Selecting University or  College drop down*/
	public static WebElement selectingUniversityOrCollege(String university) 
	{
	String xpath = "//input[@id='institute_0']/ancestor::div[@class='sWrap']/following-sibling::ul//div/span[text()='"+university+"']']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Selecting Course type from drop down*/
	public static WebElement selectingCourseType(String courseType) 
	{
	String xpath = "//input[@id='couseType_full_0']/following-sibling::label[@data-content='"+courseType+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Year of Passing drop down*/
	@FindBy(name="passingYear_0")
	private WebElement ddPassingYearHQ;
	
	/*Selecting Year of Passing from drop down*/
	public static WebElement selectingHighestQualificationPassingYear(String highestQualificationPassingYear) 
	{
	String xpath = "//input[@name='passingYear_0']/ancestor::div[@class='DDwrap']/following-sibling::div//li//span[text()='"+highestQualificationPassingYear+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Qualification  drop down*/
	@FindBy(name="qualification_1")
	private WebElement ddFirstQualification;
	
	/*Selecting Qualification from drop down*/
	public static WebElement selectingQualification(String qualification) 
	{
	String xpath = "//div[@class='transcluded']/span[text()='"+qualification+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Board  drop down*/
	@FindBy(name="schoolBoardId_1")
	private WebElement ddBoard;
	
	/*Selecting Board from drop down*/
	public static WebElement selectingBoard(String board) 
	{
	String xpath = "//div[@class='transcluded']/span[text()='"+board+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Board Passing Year  drop down*/
	@FindBy(name="schoolCompletionYear_1")
	private WebElement ddBoardYearOfPassing;
	
	/*Selecting Board Passing Year from drop down*/
	public static WebElement selectingYearBoard(String boardPassingYear) 
	{
	String xpath = "//input[@name='schoolCompletionYear_1']/ancestor::div[@class='ddwn']//div[@class='transcluded']/span[text()='"+boardPassingYear+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Medium drop down*/
	@FindBy(name="schoolMediumId_1")
	private WebElement ddBoardMedium;
	
	/*Selecting Medium from drop down*/
	public static WebElement selectingMedium(String medium) 
	{
	String xpath = "//div[@class='transcluded']/span[text()='"+medium+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/*Percentage drop down*/
	@FindBy(name="schoolPercentageId_1")
	private WebElement ddBoardPercentage;
	
	/*Selecting Percentage from drop down*/
	public static WebElement selectingPercentage(String percentage) 
	{
	String xpath = "//div[@class='transcluded']/span[text()='"+percentage+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	/** 
	 * Description: Method to select the Highest Qualification and validation
	 * @author Manikandan A
	 * @param qualification
	 * */
	public void setQualification(String qualification)
	{
		try
		{
			WebActionUtil.waitForElement(ddFirstQualification, "Qualification drop down", 5);
			WebActionUtil.isElementVisible(ddFirstQualification, "Qualification drop down");
			WebActionUtil.clickOnWebElement(ddFirstQualification, "Qualification drop down", "unable to click on Qualification drop down");
			
			WebActionUtil.waitForElement(selectingQualification(qualification), "Qualification drop down", 5);
			WebActionUtil.clickOnWebElement(selectingQualification(qualification), "Options under Qualification drop down", "Unable to click on Options under Qualification drop down");
			WebActionUtil.isElementDisplayed(selectingQualification(qualification), "Qualification drop down");
			WebActionUtil.mouseHover(driver, btnContinue);
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Qualification ");
			Assert.fail("Unable to Set Qualification");
			
		}
	}
	
	/** 
	 * Description: Method to select the Highest Qualification and validation
	 * @author Manikandan A
	 * @param board
	 * */
	public void setBoard(String board)
	{
		try
		{
			WebActionUtil.waitForElement(ddBoard, "Board drop down", 5);
			WebActionUtil.isElementVisible(ddBoard, "Board drop down");
			WebActionUtil.clickOnWebElement(ddBoard, "Board drop down", "unable to click on Board drop down");
			
			WebActionUtil.waitForElement(selectingBoard(board), "Board drop down", 5);
			WebActionUtil.clickOnWebElement(selectingBoard(board), "Options under Board drop down", "Unable to click on Options under Board drop down");
			WebActionUtil.isElementDisplayed(selectingBoard(board), "Board drop down");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Board ");
			Assert.fail("Unable to Set Board");
			
		}
	}
	
	/** 
	 * Description: Method to select the Highest Qualification and validation
	 * @author Manikandan A
	 * @param board
	 * */
	public void setBoardYearOfPassing(String boardPassingYear)
	{
		try
		{
			WebActionUtil.waitForElement(ddBoardYearOfPassing, "Board Year Passing drop down", 5);
			WebActionUtil.isElementVisible(ddBoardYearOfPassing, "Board Year Passing drop down");
			WebActionUtil.clickOnWebElement(ddBoardYearOfPassing, "Board Year Passing drop down", "unable to click on Board Year Passing drop down");
			
			WebActionUtil.waitForElement(selectingYearBoard(boardPassingYear), "Board Year Passing drop down", 5);
			WebActionUtil.clickOnWebElement(selectingYearBoard(boardPassingYear), "Options under Board Year Passing drop down", "Unable to click on Options under Board Year Passing drop down");
			WebActionUtil.isElementDisplayed(selectingYearBoard(boardPassingYear), "Board Year Passing drop down");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Board Year Passing ");
			Assert.fail("Unable to Set Board Year Passing");
			
		}
	}
	
	/** 
	 * Description: Method to select the Highest Qualification and validation
	 * @author Manikandan A
	 * @param board
	 * */
	public void setBoardMedium(String medium)
	{
		try
		{
			WebActionUtil.waitForElement(ddBoardMedium, "Board Medium drop down", 5);
			WebActionUtil.isElementVisible(ddBoardMedium, "Board Medium drop down");
			WebActionUtil.clickOnWebElement(ddBoardMedium, "Board Medium drop down", "unable to click on Board Medium drop down");
			
			WebActionUtil.waitForElement(selectingMedium(medium) , "Board Medium drop down", 5);
			WebActionUtil.clickOnWebElement(selectingMedium(medium), "Options under Board Medium drop down", "Unable to click on Options under Board Medium drop down");
			WebActionUtil.isElementDisplayed(selectingMedium(medium), "Board Medium drop down");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Board Medium");
			Assert.fail("Unable to Set Board Medium");
			
		}
	}
	
	/** 
	 * Description: Method to select the Highest Qualification and validation
	 * @author Manikandan A
	 * @param board
	 * */
	public void setBoardPercentage(String percentage)
	{
		try
		{
			WebActionUtil.waitForElement(ddBoardPercentage, "Board Percentage  drop down",5);
			WebActionUtil.clickOnWebElement(ddBoardPercentage, "Board Percentage drop down", "unable to click on Board Percentage drop down");
			
			WebActionUtil.waitForElement(selectingPercentage(percentage) , "Board Percentage drop down", 5);
			WebActionUtil.clickOnWebElement(selectingPercentage(percentage), "Options under Board Percentage drop down", "Unable to click on Options under Board Percentage drop down");
			WebActionUtil.isElementDisplayed(selectingPercentage(percentage), "Board Percentage drop down");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Board Percentage");
			Assert.fail("Unable to Set Board Percentage");
			
		}
	}
	/*@FindBy(name="passingYear_1")
	private WebElement ddFirstPassingYear;
	
	public static WebElement selectingFirstQualificationPassingYear(String firstQualificationPassingYear) 
	{
	String xpath = "//input[@name='passingYear_1']/ancestor::div[@class='DDwrap']/following-sibling::div//li//span[text()='"+firstQualificationPassingYear+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	@FindBy(name="course_1")
	private WebElement ddFirstCouse;
	
	@FindBy(name="spec_1")
	private WebElement ddFirstSpecialization ;
	
	public static WebElement selectingFirstQualification(String firstQualification) 
	{
	String xpath = "//input[@name='qualification_1']/ancestor::div[@class='DDwrap']/following-sibling::div//li//span[text()='"+firstQualification+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	public static WebElement selectUniversityOrCollege(String university) 
	{
	String xpath = "//input[@id='institute_0']/ancestor::div[@class='sWrap']/following-sibling::ul/li//span[text()='"+university+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	public static WebElement selectingFirstCourse(String firstCourse) 
	{
	String xpath = "//input[@name='course_1']/ancestor::div[@class='DDwrap']/following-sibling::div//li//span[text()='"+firstCourse+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	public static WebElement selectingFirstSpecialization(String firstSpecialization) 
	{
	String xpath = "//input[@name='spec_1']/ancestor::div[@class='DDwrap']/following-sibling::div//li//span[text()='"+firstSpecialization+"']";
	return driver.findElement(By.xpath(xpath));
	}
	
	@FindBy(name="institute_1")
	private WebElement tbFirstUniversityOrCollege;
	
	public static WebElement selectingFirstCourseType(String firstCourseType) 
	{
	String xpath = "//input[@id='couseType_full_1']/following-sibling::label[@data-content='"+firstCourseType+"']";
	return driver.findElement(By.xpath(xpath));
	}*/
	
	/** 
	 * Description: Method to click Add More Education
	 * @author Manikandan A
	 */	
	public void clickOnAddMoreEducationBtn()
	{
		try
		{
			WebActionUtil.waitForElement(lnkAddMoreEducation, "Add More Educaion Link", 5);
			WebActionUtil.isElementClickable(lnkAddMoreEducation, "Add More Educaion Link");
			WebActionUtil.clickOnWebElement(lnkAddMoreEducation, "Add More Educaion Link", "unable to click on Add More Educaion Link");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("unable to click on Add More Education Link");
			Assert.fail("unable to click on Add More Educaiton Link");
			
		}
	}
	
	/** 
	 * Description: Method to select the Highest Qualification and validation
	 * @author Manikandan A
	 * @param highestQualification
	 * */
	public void setHighestQualification(String highestQualification)
	{
		try
		{
			WebActionUtil.waitForElement(ddHighestQualification, "Highest Qualification drop down", 5);
			WebActionUtil.isElementVisible(ddHighestQualification, "Highest Qualification drop down");
			WebActionUtil.clickOnWebElement(ddHighestQualification, "Highest Qualification drop down", "unable to click on Highest Qualification drop down");
			
			WebActionUtil.waitForElement(selectingHighestQualification(highestQualification), "Highest Qualification drop down", 5);
			WebActionUtil.clickOnWebElement(selectingHighestQualification(highestQualification), "Options under Highest Qualification drop down", "Unable to click on Options under Highest Qualification drop down");
			WebActionUtil.isElementDisplayed(selectingHighestQualification(highestQualification), "Highest Qualification drop down");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Highest Qualification ");
			Assert.fail("Unable to Set Highest Qualification");
			
		}
	}
	
	/** 
	 * Description: Method to select the Course and validation
	 * @author Manikandan A
	 * @param course
	 * */
	public void setCourseForHQ(String course)
	{
		try
		{
			WebActionUtil.waitForElement(ddCourse, "Highest Qualification drop down", 5);
			WebActionUtil.clickOnWebElement(ddCourse, "Course drop down under Highest Qualification", "unable to click on Course drop down under Highest Qualification");
			
			
			WebActionUtil.waitForElement(selectingCourse(course), "Highest Qualification drop down", 5);
			WebActionUtil.clickOnWebElement(selectingCourse(course), "Options of Course drop down under Highest Qualification", "Unable to click on Options of Course drop down under Highest Qualification");
			WebActionUtil.isElementDisplayed(selectingCourse(course), " Course drop down under Highest Qualification");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Course");
			Assert.fail("Unable to Set Course");
			
		}
	}
	
	/** 
	 * Description: Method to select the Specialization and validation
	 * @author Manikandan A
	 * @param course
	 * */
	public void setSpecialization(String specialization)
	{
		try
		{
			WebActionUtil.waitForElement(ddSpecialization, "Specialization drop down under Highest Qualification", 5);
			WebActionUtil.clickOnWebElement(ddSpecialization, "Specialization drop down under Highest Qualification", "unable to click on Specialization drop down under Highest Qualification");
			
			WebActionUtil.isElementClickable(selectingSpecialization(specialization), "Options under Specialization drop down under Highest Qualification" );
			WebActionUtil.clickOnWebElement(selectingSpecialization(specialization), "Options under Specialization drop down under Highest Qualification", "Unable to click on Options of Specialization drop down under Highest Qualification");
			WebActionUtil.isElementDisplayed(selectingSpecialization(specialization), " Specialization drop down under Highest Qualification");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Specialization");
			Assert.fail("Unable to Set Specialization");
			
		}
	}
	
	/** 
	 * Description: Method to select the University and validation
	 * @author Manikandan A
	 * @param university
	 * */
	public void setUniversityOrCollege(String university)
	{
		try
		{
			WebActionUtil.isElementVisible(tbUniversityOrCollege, "University or College Text Box");
			WebActionUtil.typeText(tbUniversityOrCollege,university , "University or College Text Box");
			//WebActionUtil.typeTextUsingJS(tbCurrentCity, "current City",currentCity );
			String actualData=tbUniversityOrCollege.getAttribute("value");
			WebActionUtil.validateEnteredData(university, actualData,"Current City Text box",university + " University or CollegeText box", university +" University or College Text box");
				}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("unable to set the University ");
			Assert.fail("unable to set the University");
		}
		
		
	}
	
	/** 
	 * Description: Method to select the Course Type and validation
	 * @author Manikandan A
	 * @param courseType
	 * */
	public void clickOnCourseType(String courseType)
	{
		try
		{
			WebActionUtil.isElementClickable(selectingCourseType(courseType), "Course type radio button under Highest Qualification");
			WebActionUtil.clickOnWebElement(selectingCourseType(courseType), "Course type radio button under Highest Qualification", "unable to click on Course type radio button under Highest Qualification ");
			WebActionUtil.isElementDisplayed(selectingCourseType(courseType), " Course type radio button under Highest Qualification");
			
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Course Type");
			Assert.fail("Unable to Set Course Type");
			
		}
	}
	
	/** 
	 * Description: Method to select the Course Type and validation
	 * @author Manikandan A
	 * @param passingYear
	 * */
	public void setPassingYearForHigherEducation(String passingYear)
	{
		try
		{
			WebActionUtil.isElementClickable(ddPassingYearHQ, "Passing Year Drop Down under Highest Qualification");
			WebActionUtil.clickOnWebElement(ddPassingYearHQ, "Passing Year Drop Down under Highest Qualification", "unable to click on Passing Year Drop Down under Highest Qualification ");
			
			WebActionUtil.clickOnWebElement(selectingHighestQualificationPassingYear(passingYear), "Option in Passing Year Drop Down under Highest Qualification", "unable to click on option in Passing Year Drop Down under Highest Qualification");
			WebActionUtil.isElementDisplayed(selectingHighestQualificationPassingYear(passingYear), " Option in Passing Year Drop Down under Highest Qualificatio");
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("Unable to Set Passing year");
			Assert.fail("Unable to Set Passing year");
			
		}
	}
	
	public void clickOnContinueBtn()
	{
		try
		{
			WebActionUtil.isElementClickable(btnContinue, "Continue Button");
			WebActionUtil.clickOnWebElement(btnContinue, "Continue Button", "unable to click on Continue Button");
			
			
		}
		catch(Exception e)
		{
			WebActionUtil.error(e.getMessage());
			WebActionUtil.error("unable to click on Continue Button");
			Assert.fail("unable to click on Continue Button");
			
		}
	}
	
	
}

