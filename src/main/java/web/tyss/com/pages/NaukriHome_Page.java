package web.tyss.com.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import web.tyss.com.util.WebActionUtil;

public class NaukriHome_Page {
	public static WebDriver driver;
	public WebActionUtil WebActionUtil;
	public long ETO = 10;

	public NaukriHome_Page(WebDriver driver, long ETO, WebActionUtil WebActionUtil) 
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.WebActionUtil = WebActionUtil;
		this.ETO = ETO;
	}
	
	/*Cookies Button*/
	@FindBy(xpath = "//div/div[@class='ppContainer']/button")
	private  WebElement btnGotIt;
	
	/*Location Pop up*/
	@FindBy(xpath = "//div[@id='geoLocPopUp']")
	private  WebElement lnkPopup;
	
	/*Text of Location Pop up*/
	@FindBy(xpath = "//p[@class='caption']")
	private  WebElement txtPopup;
	
	/*Text of Privacy & Cookies Policy*/
	@FindBy(xpath = "//div[@class='ppTxt']")
	private  WebElement txtPrivacyCookies;
	
	/*Later Button in Pop up*/
	@FindBy(xpath = "//span[@id='block']")
	private  WebElement btnPopUpLater;
	
	/*Tell us your Work Status pop up*/
	@FindBy(xpath="//div[contains(text(),'Tell us your Work Status')]")
	private WebElement txtTellUsYourWorkStatusPopUp;
	
	/*I am Experienced Button*/
	@FindBy(xpath="//button[contains(text(),'I am Experienced')]")
	private WebElement btnIAmExperienced;
	
	/*Register for free Button*/
	@FindBy(xpath="//div[@class='wdgt-register-btn']/button")
	private WebElement btnRegisterForFree;
	
	
		/** 
		 * 
		 * Description: Method to close window pop ups
		 * @author Manikandan A
		 */
		public synchronized void closeWindowPopUps() {
			try {
				
				Thread.sleep(10000);
				WebActionUtil.closeWindowPopup();
				}
			catch (Exception e) 
			{
				 WebActionUtil.error(e.getMessage());
				WebActionUtil.error("Unable to close Window PopUps");
				Assert.fail("Unable to close Window PopUps");
			}
		}
		
		/** 
		 * 
		 * Description: Method to accept Cookies
		 * @author Manikandan A
		 */		
		public synchronized void clkOnPrivacyPolicy() {
			try {
				
				WebActionUtil.waitForElement(txtPrivacyCookies, "Privacy Policy & Cookie Policy", 20);
				WebActionUtil.verifytext("We use cookies to improve your experience. By continuing to browse the site, you agree to our Privacy Policy & Cookie Policy.", txtPrivacyCookies, "Privacy Policy & Cookie Policy");
				WebActionUtil.clickOnWebElement(btnGotIt, "Privacy Policy & Cookie Policy", "Clicked on GOT IT button");
				WebActionUtil.info("Clicked on GOT IT button");
			}
			catch (Exception e) 
			{
				 WebActionUtil.error(e.getMessage());
				WebActionUtil.error("Unable to click on Got It button");
				Assert.fail("Unable to click on Got It button");
			}
		}
		
		/** 
		 * 
		 * Description: Method to click on PopUp Later Button
		 * @author Manikandan A
		 */
		
		public synchronized void clkLaterOnPopup() {
			try {
				WebActionUtil.waitForElement(txtPopup, "Location Pop up", 20);
				WebActionUtil.verifytext("Share your location with Naukri.com for more relevant jobs", txtPopup, "Location Popup");
				WebActionUtil.isElementVisible(btnPopUpLater, "Location Pop up");
				WebActionUtil.clickOnWebElement(btnPopUpLater, "Later button in Popup", "Clicked on Later button");
				WebActionUtil.info("Clicked on Later button");
			
			}
			catch (Exception e) 
			{
				 WebActionUtil.error(e.getMessage());
				WebActionUtil.error("Unable to click on Later button");
				Assert.fail("Unable to click on Later button");
			}
	}
		
		
		/** 
		 * 
		 * Description: method to verify Tell us Your Work status
		 * @author Manikandan A
		 */
		
		public synchronized void verifyTxtTellUsYourWorkStatus() {
			try {
				WebActionUtil.waitForElement(txtTellUsYourWorkStatusPopUp, "Tell us your Work Status Pop Up text", 20);
				WebActionUtil.verifytext("Tell us your Work Status", txtTellUsYourWorkStatusPopUp, "Tell us your Work Status Pop Up text is visible");
			}
			catch (Exception e) 
			{
				 WebActionUtil.error(e.getMessage());
				WebActionUtil.error("Tell us your Work Status Pop Up text is not visible");
				Assert.fail("Tell us your Work Status Pop Up text is not visible");
			}
		}
		

		/** 
		 * 
		 * Description: method to click on Register for free Button
		 * @author Manikandan A
		 */
		
		public synchronized void clkOnRegisterForFreeBtn() {
			try {
				WebActionUtil.waitForElement(btnRegisterForFree, "Register for Free button", 10);		
				WebActionUtil.clickOnWebElement(btnRegisterForFree, "Register for free button", "Unable to click on Register for free button");
				WebActionUtil.info("Register for free button");
			}
			catch (Exception e) 
			{
				 WebActionUtil.error(e.getMessage());
				WebActionUtil.error("Unable to click on Register for free button");
				Assert.fail("Unable to click on Register for free button ");
			}
		}
		
		/** 
		 * 
		 * Description: Method to click on I am Experienced Button
		 * @author Manikandan A
		 */
		
		public synchronized void clkOnIAmExperiencedBtn() {
			try {
				
				verifyTxtTellUsYourWorkStatus();
				WebActionUtil.waitForElement(btnIAmExperienced, "I am Experienced Button", 5);
				WebActionUtil.clickOnWebElement(btnIAmExperienced, "I am Experienced Button", "Unable to click on I am Experienced Button");
				WebActionUtil.info("clicked on I am Experienced Button");
			}
			catch (Exception e) 
			{
				 WebActionUtil.error(e.getMessage());
				WebActionUtil.error("Unable to click on Im Experienced Button");
				Assert.fail("Unable to click on Im Experienced Button");
			}
		}
}

