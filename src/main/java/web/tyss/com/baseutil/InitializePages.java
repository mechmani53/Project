package web.tyss.com.baseutil;

import org.openqa.selenium.WebDriver;

import web.tyss.com.pages.AdditionalDetails_Page;
import web.tyss.com.pages.Ajio_Page;
import web.tyss.com.pages.Bluestone_Home_Page;
import web.tyss.com.pages.BuyProduct_Page;
import web.tyss.com.pages.Buy_Product_page;
import web.tyss.com.pages.Decathlon_Page;
import web.tyss.com.pages.EducationDetails_Page;
import web.tyss.com.pages.EmploymentDetails_Page;
import web.tyss.com.pages.Everton_Page;
import web.tyss.com.pages.Login_Page;
import web.tyss.com.pages.NaukriHome_Page;
import web.tyss.com.pages.NewProject_Page;
import web.tyss.com.pages.Nike_Page;
import web.tyss.com.pages.Nykaa_Page;
import web.tyss.com.pages.Payment_Page;
import web.tyss.com.pages.PepperFry_Page;
import web.tyss.com.pages.PersonalFormFill_Page;
import web.tyss.com.pages.PlaceOrder_Page;
import web.tyss.com.pages.Tasks_Page;
import web.tyss.com.pages.Users_Page;
import web.tyss.com.util.WebActionUtil;

/**
 * Description Initialize all pages with driver,ETO, WebAactionUtil
 * 
 * @author Manikandan A
 * 
 */

public class InitializePages {
	public Login_Page loginPage;
	public Users_Page usersPage;
	public Bluestone_Home_Page homePage;
	public Buy_Product_page productPage;
	public PlaceOrder_Page orderPage;
	public Payment_Page paymentPage;
	public BuyProduct_Page prodPage;
	public Nykaa_Page nykaaPage;
	public PepperFry_Page pepperFryPage;
	public Tasks_Page tasksPage;
	public NewProject_Page projectPage;
	public Nike_Page nikePage;
	public Everton_Page evertonPage;
	public Ajio_Page ajioPage;
	public Decathlon_Page decathlonPage;
	public NaukriHome_Page naukriHomePage;
	public PersonalFormFill_Page personalFormFillPage;
	public EmploymentDetails_Page employmentDetailsPage;
	public EducationDetails_Page educationDetailsPage;
	public AdditionalDetails_Page additionalDetailsPage;
	
	public InitializePages(WebDriver driver, long ETO, WebActionUtil WebActionUtil) {

		loginPage = new Login_Page(driver, ETO, WebActionUtil);
		usersPage = new Users_Page(driver, ETO, WebActionUtil);
		
		homePage = new Bluestone_Home_Page(driver, ETO, WebActionUtil);
		productPage = new Buy_Product_page(driver, ETO, WebActionUtil);
		prodPage = new BuyProduct_Page(driver, ETO, WebActionUtil);
		orderPage = new PlaceOrder_Page(driver, ETO, WebActionUtil);
		paymentPage = new Payment_Page(driver, ETO, WebActionUtil);

		nykaaPage = new Nykaa_Page(driver, ETO, WebActionUtil);
		
		pepperFryPage = new PepperFry_Page(driver, ETO, WebActionUtil);
		tasksPage = new Tasks_Page(driver, ETO, WebActionUtil);
		projectPage = new NewProject_Page(driver, ETO, WebActionUtil);
		evertonPage = new Everton_Page(driver, ETO, WebActionUtil);
		ajioPage =  new Ajio_Page(driver, ETO, WebActionUtil);
		nikePage = new Nike_Page(driver, ETO, WebActionUtil);
		decathlonPage= new Decathlon_Page(driver, ETO, WebActionUtil);
		
		naukriHomePage=new NaukriHome_Page(driver, ETO, WebActionUtil);
		personalFormFillPage =new PersonalFormFill_Page(driver, ETO, WebActionUtil);
		employmentDetailsPage=new EmploymentDetails_Page(driver, ETO, WebActionUtil);
		educationDetailsPage=new EducationDetails_Page(driver, ETO, WebActionUtil);
		additionalDetailsPage=new AdditionalDetails_Page(driver, ETO, WebActionUtil);
	}

}
